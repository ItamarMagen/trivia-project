﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace client2
{
    /// <summary>
    /// Interaction logic for MenuWindow.xaml
    /// </summary>
    public partial class MenuWindow : Window
    {
        public MenuWindow(string user)
        {
            InitializeComponent();
            Username_Box.Text = user;
            
        }
        private void Sign_Out_Button(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void Join_Room_Button(object sender, RoutedEventArgs e)
        {
            Hide();
            JoinRoomWindow join_room_window = new JoinRoomWindow();
            join_room_window.ShowDialog();
            ShowDialog();
        }
        private void Create_Room_Button(object sender, RoutedEventArgs e)
        {
            Hide();
            CreateRoomWindow create_room_window = new CreateRoomWindow();
            create_room_window.ShowDialog();
            ShowDialog();
        }
        private void My_Status_Button(object sender, RoutedEventArgs e)
        {
            
        }
        private void Best_Scores_Button(object sender, RoutedEventArgs e)
        {
            
        }

        private void Quit_Button(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
