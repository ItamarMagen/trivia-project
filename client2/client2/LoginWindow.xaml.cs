﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Net;
using System.Net.Sockets;
using serializer;



namespace client2
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        Socket sock;
        bool isUserSignedIn = false;
        public LoginWindow()
        {
            InitializeComponent();
           
            sock = connecter.Connect.ConnectToServer(Socket_Details.IP, Socket_Details.PORT);
            if(!sock.Connected)
            {
                System.Windows.MessageBox.Show("You don't have runnig server.");
                Close();
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Sign_In_Button(object sender, RoutedEventArgs e)
        {
            LoginRequest req = new LoginRequest();

            string username = username_box.Text;
            string password = password_box.Password;
       
            List<byte> buffer = null;
            List<byte> response_buffer = null;

            bool CapitalFlag = false;// Capital letter flag
            bool SmallFlag = false;// Small letter flag
            bool NumberFlag = false;//Number letter flag  
            bool isLoginSucceed = false;

            if (username != "" && password != "")
            {
                if (password.Length < 4)
                {
                    //System.Windows.MessageBox.Show(password.Length.ToString());
                    System.Windows.MessageBox.Show("Invalid password, password must contain at least 4 characters," +
                        "\ncapital letter, small letter and a number.");
                }
                else
                {
                    for (int i = 0; i < password.Length; i++)
                    {
                        if (password[i] >= 'A' && password[i] <= 'Z')
                            CapitalFlag = true;
                        else if (password[i] >= 'a' && password[i] <= 'z')
                            SmallFlag = true;
                        else if (password[i] >= '0' && password[i] <= '9')
                            NumberFlag = true;
                    }
                    if (CapitalFlag && SmallFlag && NumberFlag) //If password have small letter, capital letter and a number
                    {
                        
                        req.username = username;
                        req.password = password;
                        buffer = serializer.serialize.CreateLoginRequest(req);
                        connecter.Connect.SendMessage(sock, buffer);
                        //response_buffer = connecter.Connect.ReciveMessage(sock);
                        isLoginSucceed = true;
                        if (isLoginSucceed)
                        {
                            Hide();
                            MenuWindow menu_window = new MenuWindow(username);
                            menu_window.ShowDialog();
                            ShowDialog();
                        }
                    } 
                    else
                    {
                        System.Windows.MessageBox.Show("Invalid password, password must contain at least 4 characters," +
                           "\ncapital letter, small letter and a number.");
                        CapitalFlag = false;// Capital letter flag
                        SmallFlag = false;// Small letter flag
                        NumberFlag = false;//Number letter flag
                    }
                }
            }
            else
            {
                System.Windows.MessageBox.Show("Please fill all fields.");
            }

        }
        private void Sign_Up_Button(object sender, RoutedEventArgs e)
        {
            Hide();
            new SignupWin().ShowDialog();
            ShowDialog();
        }

        private void Back_Button(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.MessageBox.Show("ouch");
        }
    }
}
