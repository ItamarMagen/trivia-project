﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace client2
{
    /// <summary>
    /// Interaction logic for JoinRoomWindow.xaml
    /// </summary>
    public partial class JoinRoomWindow : Window
    {
        public JoinRoomWindow()
        {
            InitializeComponent();
        }

        private void Refresh_Button(object sender, RoutedEventArgs e)
        {

        }

        private void Join_Button(object sender, RoutedEventArgs e)
        {
            //System.Windows.MessageBox.Show("First you need to choose a room.");
        }

        private void Back_Buttton(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
