﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace deserializer
{
    class deserializer
    {
        public class SignupResponse // version 1
        {
            public uint status;
        }

        public class LoginResponse // version 1
        {
            public uint status;
        }

        public class ErrorResponse // version 1
        {
            public string message;
        }

        public class LogoutResponse // version 2
        {
            public uint status;
        }
        /*
        public class GetRoomResponse // version 2
        {
            public uint status;
            public List<RoomData> rooms = new List<RoomData>();
        }
        */
        public class GetPlayersInRoomResponse // version 2
        {
            public List<string> players = new List<string>();
        }

        public class JoinRoomResponse // version 2
        {
            public uint status;
        }

        public class CreateRoomResponse // version 2
        {
            public uint status;
        }
        /*
        public class HighscoreResponse // version 2
        {
            public uint status;
            public List<Highscore> highscores = new List<Highscore>();
        }
        */
        public class CloseRoomResponse // version 3
        {
            public uint status;
        }

        public class StartGameResponse // version 3
        {
            public uint status;
        }

        public class GetRoomStateResponse // version 3
        {
            public uint status;
            public bool hasGameBegun;
            public List<string> players = new List<string>();
            public uint questionCount;
            //answerTimeout
        }

        public class LeaveRoomResponse // version 3
        {
            public uint status;
        }

        public class GetQuestionResponse // version 4
        {
            public uint status;
            public string question;
            public SortedDictionary<uint, string> answers = new SortedDictionary<uint, string>();
        }

        public class SubmitAnswerResponse // version 4
        {
            public uint status;
            public uint correctAnswerId;
        }

        public class PlayerResults // version 4
        {
            public string username;
            public uint correctAnswerCount;
            public uint intwrongAnswerCount;
            public uint averageAnswerTime;
        }

        public class GetGameResultsResponse // version 4
        {
            public uint status;
            public List<PlayerResults> results = new List<PlayerResults>();

        }
    }
}
