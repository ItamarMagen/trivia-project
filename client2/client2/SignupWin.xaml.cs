﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.Net;
using System.Net.Sockets;
using serializer;


/*
 namespace Client
{
    static class socket_details
    {
        public const string IP = "127.0.0.1";
        public const int PORT = 54000;
    }
    class Program
    {
        public static Socket ConnectToServer(string ipHost, int port)
        {
            Socket sender = null;
            try
            {
                IPAddress ipAddr = IPAddress.Parse(ipHost);
                IPEndPoint localEndPoint = new IPEndPoint(ipAddr, port);
                sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                sender.Connect(localEndPoint);
                Console.WriteLine("Socket connected to -> {0} ", sender.RemoteEndPoint.ToString());

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return sender;
        }

        public static void SendMessage(Socket sender, string strMsg)
        {
            byte[] messageSent = Encoding.ASCII.GetBytes(strMsg);
            try
            {
                int byteSent = sender.Send(messageSent);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static string ReciveMessage(Socket sender)
        {
            byte[] messageReceived = new byte[1024];
            string strMessageRecived = "";
            int byteRecv = sender.Receive(messageReceived);
            strMessageRecived = Encoding.ASCII.GetString(messageReceived, 0, byteRecv);
            Console.WriteLine("Message from Server -> {0}", strMessageRecived);
            return strMessageRecived;
        }
    }
}
*/
namespace client2
{
    /// <summary>
    /// Interaction logic for SignupWin.xaml
    /// </summary>
    public partial class SignupWin : Window
    {
        public SignupWin()
        {
            InitializeComponent();
            //Socket s = Client.Program.ConnectToServer(Client.socket_details.IP, Client.socket_details.PORT);
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
    
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string username = Username_box.Text;
            string password = Password_box.Text;
            string email = Email_box.Text;
            string RequestCode = "";
            string data = "";
            string msg = "";
            string responseMsg = "";
            int Data_Length = 0;

            bool CapitalFlag = false;// Capital letter flag
            bool SmallFlag = false;// Small letter flag
            bool NumberFlag = false;//Number letter flag  

            char[] temp = new char[4];

            Socket s = null;
            if (username != "" && password != "" && email != "")
            {
                if (password.Length < 4)
                {
                    //System.Windows.MessageBox.Show(password.Length.ToString());
                    System.Windows.MessageBox.Show("Invalid password, password must contain at least 4 characters," +
                        "\ncapital letter, small letter and a number.");
                }
                for (int i = 0; i < password.Length; i++)
                {
                    if (password[i] >= 'A' && password[i] <= 'Z')
                        CapitalFlag = true;
                    if (password[i] >= 'a' && password[i] <= 'z')
                        SmallFlag = true;
                    if (password[i] >= '0' && password[i] <= '9')
                        NumberFlag = true;
                }
                if (!CapitalFlag && !SmallFlag && !NumberFlag)
                {
                    System.Windows.MessageBox.Show("Invalid password, password must contain at least 4 characters," +
                       "\ncapital letter, small letter and a number.");
                    CapitalFlag = false;// Capital letter flag
                    SmallFlag = false;// Small letter flag
                    NumberFlag = false;//Number letter flag
                }
                else
                {
                    SignupRequest sur = new SignupRequest();
                    sur.username = username;
                    sur.password = password;
                    sur.email = email;
                    RequestCode = "2";                
                    data = JsonConvert.SerializeObject(sur);
                    SignupRequest deserializedProduct = JsonConvert.DeserializeObject<SignupRequest>(data);
                    Data_Length = data.Length;// get data bytes length

                    //System.Windows.MessageBox.Show(data + Data_Length);
                    // byte[] msg = Encoding.UTF8.GetBytes(RequestCode + Data_Length + data);
                    /*Console.WriteLine("0" + temp[0]);
                    Console.WriteLine("1" + temp[1]);
                    System.Windows.MessageBox.Show("check");*/
                    //byte[] msg = Encoding.UTF8.GetBytes(RequestCode + temp +  Data_Length.ToString() + data);
                    //System.Windows.MessageBox.Show(Encoding.Default.GetString(msg));


                    try
                    {
                       // connecter.Connect.SendMessage(s, msg);
                       // responseMsg = Client.Program.ReciveMessage(s);
                        s.Shutdown(SocketShutdown.Both);
                        s.Close();
                        DialogResult = false;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                    this.Close();
                }
            }
            else
            {
                System.Windows.MessageBox.Show("Please fill all fields.");
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        { 
            DialogResult = false;
        }
    }
}
