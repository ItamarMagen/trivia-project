﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace connecter
{
    public static class Connect
    {
        
        public static Socket ConnectToServer(string ipHost, int port)
        {
            Socket sender = null;
            try
            {
                IPAddress ipAddr = IPAddress.Parse(ipHost);
                IPEndPoint localEndPoint = new IPEndPoint(ipAddr, port);
                sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                sender.Connect(localEndPoint);
                Console.WriteLine("Socket connected to -> {0} ", sender.RemoteEndPoint.ToString());

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return sender;
        }

        public static void SendMessage(Socket sender, List<byte> msg)
        {
            try
            {
                int byteSent = sender.Send(msg.ToArray());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static List<byte> ReciveMessage(Socket sender)
        {
            byte[] messageReceived = new byte[1024];
            int byteRecv = sender.Receive(messageReceived);
            return messageReceived.ToList();
        }
    }
}
