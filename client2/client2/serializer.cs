﻿using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.Net;
using System.Net.Sockets;

namespace serializer
{
    public enum Requests_Codes
    {
        LOGIN_REQUEST = 250,
        SINGUP_REQUEST,
        GET_PLAYERS_IN_THE_ROOM_REQUEST,
        JOIN_ROOM_REQUEST,
        CREATE_ROOM_REQUEST,
        SUBMIT_ANSWER_REQUEST,
    }

    public struct LoginRequest
    {
        public string username;
        public string password;
    }
    public struct SignupRequest
    {
        public string username;
        public string password;
        public string email;
    }

    public struct GetPlayersInRoomRequest
    {
        public int roomId;
    }

    public struct JoinRoomRequest
    {
        public int roomId;
    }

    public struct CreateRoomRequest
    {
        public string roomName;
        public int maxUsers;
        public int questionCount;
        public int answerTimeout;
    }

    public struct SubmitAnswerRequest
    {
        public int answerId;
    }

    public static class serialize
    {
        /*
        Function converts a size of a data to bytes
        */
        public static List<byte> GetSize(int paramInt)
        {
            byte[] buff = new byte[4];
            for (int i = 0; i < 4; i++)
            {
                buff[3 - i] = (byte)(paramInt >> (i * 8));
            }
            return new List<byte>(buff);
        }

        /*
        Function converts a code of a message to bytes
        */
        public static List<byte> GetCode(int paramInt)
        {
            byte[] buff = new byte[1];
            buff[0] = (byte)paramInt;
            return new List<byte>(buff);
        }

        public static List<byte> CreateLoginRequest(LoginRequest req)
        {
            List<byte> buffer = new List<byte>();
            List<byte> buffer_data = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(req)).ToList();
            List<byte> buffer_size = GetSize(JsonConvert.SerializeObject(req).Length);
            List<byte> request_code = GetCode((int)Requests_Codes.LOGIN_REQUEST);
            //std::cout << "size of response code "<< sizeof(buffer_size[0]) * buffer_size.size()<<" response_code "<< static_cast<int>(response_code[0]);
            buffer.AddRange(request_code);
            buffer.AddRange(buffer_size);
            buffer.AddRange(buffer_data);
            return buffer;
        }
        public static List<byte> CreateSignupRequest(SignupRequest req)
        {
            List<byte> buffer = new List<byte>();
            List<byte> buffer_data = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(req)).ToList();
            List<byte> buffer_size = GetSize(JsonConvert.SerializeObject(req).Length);
            List<byte> request_code = GetCode((int)Requests_Codes.SINGUP_REQUEST);
            //std::cout << "size of response code "<< sizeof(buffer_size[0]) * buffer_size.size()<<" response_code "<< static_cast<int>(response_code[0]);
            buffer.AddRange(request_code);
            buffer.AddRange(buffer_size);
            buffer.AddRange(buffer_data);
            return buffer;
        }
        public static List<byte> GetPlayersRequest(GetPlayersInRoomRequest req)
        {
            List<byte> buffer = null;
            List<byte> buffer_data = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(req)).ToList();
            List<byte> buffer_size = GetSize(JsonConvert.SerializeObject(req).Length);
            List<byte> request_code = GetCode((int)Requests_Codes.GET_PLAYERS_IN_THE_ROOM_REQUEST);
            //std::cout << "size of response code "<< sizeof(buffer_size[0]) * buffer_size.size()<<" response_code "<< static_cast<int>(response_code[0]);
            buffer.AddRange(request_code);
            buffer.AddRange(buffer_size);
            buffer.AddRange(buffer_data);
            return buffer;
        }
        public static List<byte> JoinTheRoomRequest(JoinRoomRequest req)
        {
            List<byte> buffer = null;
            List<byte> buffer_data = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(req)).ToList();
            List<byte> buffer_size = GetSize(JsonConvert.SerializeObject(req).Length);
            List<byte> request_code = GetCode((int)Requests_Codes.JOIN_ROOM_REQUEST);
            //std::cout << "size of response code "<< sizeof(buffer_size[0]) * buffer_size.size()<<" response_code "<< static_cast<int>(response_code[0]);
            buffer.AddRange(request_code);
            buffer.AddRange(buffer_size);
            buffer.AddRange(buffer_data);
            return buffer;
        }
        public static List<byte> CreateRoomRequest(CreateRoomRequest req)
        {
            List<byte> buffer = null;
            List<byte> buffer_data = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(req)).ToList();
            List<byte> buffer_size = GetSize(JsonConvert.SerializeObject(req).Length);
            List<byte> request_code = GetCode((int)Requests_Codes.CREATE_ROOM_REQUEST);
            //std::cout << "size of response code "<< sizeof(buffer_size[0]) * buffer_size.size()<<" response_code "<< static_cast<int>(response_code[0]);
            buffer.AddRange(request_code);
            buffer.AddRange(buffer_size);
            buffer.AddRange(buffer_data);
            return buffer;
        }
        public static List<byte> SubmitAnswerRequest(SubmitAnswerRequest req)
        {
            List<byte> buffer = null;
            List<byte> buffer_data = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(req)).ToList();
            List<byte> buffer_size = GetSize(JsonConvert.SerializeObject(req).Length);
            List<byte> request_code = GetCode((int)Requests_Codes.SUBMIT_ANSWER_REQUEST);
            //std::cout << "size of response code "<< sizeof(buffer_size[0]) * buffer_size.size()<<" response_code "<< static_cast<int>(response_code[0]);
            buffer.AddRange(request_code);
            buffer.AddRange(buffer_size);
            buffer.AddRange(buffer_data);
            return buffer;
        }
    }
}